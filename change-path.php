<?php

$ehrconnect_path = "C:\\MedImage\\addons\\ehrconnect\\";

include_once($ehrconnect_path . "db-wrapper.php");


//Read the config file to get parameters
$json_str = file_get_contents(realpath ($ehrconnect_path . 'config\\ehrconnect.json'));
$json = json_decode($json_str, true); // decode the JSON into an associative array
$json = backwards_compatible($json);	//ensure any old style entries are converted into the new style


echo "\n\nMedImage EHR Path Changer for MedTech Evolution\n==================================================\n\nThis script will change your MedTech Evolution installation's historical MedImage photo path, to a new one.  We recommend backing up your Evolution SQL database before running this script. Please note: this script is currently a Beta, and is experimental. Use this at your own risk.  A log will be created in the current folder called change-path.log, if you are running this script with the correct Admin permissions.\n\nDo you want to continue? (y/N)";


if (!in_array(trim(fgets(STDIN)), array('y', 'Y'))) {
    echo 'Stopped' . "\n";
    exit(0);
}

if(db_code_checks($json)) {

	try {
		$dbh = db_connect($json);
		if(!$dbh) {
			$errmsg = db_err_msg($json);
			
			echo "\nSorry, we could not connect to the EHR system database. Please check your username, password, and other parameters.";
		} else {
		
			//Collect old path
			echo "\nPlease enter your OLD photo path (e.g. \\\\yourserver\\yourfolder\\ or M:\\yourfolder\\).\nNote: Be specific with capital letters.\n";
			$old_path = trim(fgets(STDIN));
			echo "Your old path: " . $old_path;
			
			//Collect new path
			echo "\nPlease enter your NEW photo path (e.g. \\\\yourserver\\yourfolder\\ or M:\\yourfolder\\) that you want the old photo path to be replaced with.\nNote: Be specific with capital letters.\n";
			$new_path = trim(fgets(STDIN));
			echo "Your new path: " . $new_path;
			
			echo "\n\nDo you want to continue with a replacement? (y/N)";

			if (!in_array(trim(fgets(STDIN)), array('y', 'Y'))) {
				 echo 'Stopped' . "\n";
				 exit(0);
			}
			
			//Get the last manually entered photo on this server
			$sql = "SELECT ID, PathToFile FROM ATTACHMENTSMGR WHERE Author = 'MedImage' OR Note = 'Added by MedImage'";
			
			$success = true;
			
			//Run through each query
			echo "\nRunning query to find the latest Attachments Manager entry:  " . $sql;
			$db_result = db_query($json, $dbh, $sql) or die("Sorry, the MedImage Add-on could not query the EHR system. Please check your user has 'read' permissions. Trying to run the query: " . $sql);	
			
			//Collect the data into an associative PHP array.  WARNING: we are using the ODBC connection, rather than our db-wrapper. Using the SQL server connection may break this.
			$result = array();
			while($row = odbc_fetch_row($db_result)) {
				
				$old_database_path = odbc_result($db_result, 'PathToFile');
				$id = odbc_result($db_result, 'ID');
				echo "\nRead entry: " . $old_database_path . "  ID:" . $id; 
				$new_database_path = str_replace($old_path, $new_path, $old_database_path);
				
				if($new_database_path != $old_database_path) {
					$sql = "UPDATE ATTACHMENTSMGR SET PathToFile = '" . $new_database_path . "' WHERE ID = " . $id;
					db_query($json, $dbh, $sql) or die("Sorry, the MedImage Add-on could not update the EHR system. Please check your user has 'update' permissions. Trying to run the query: " . $sql);	
					$msg = "\nRan query: " . $sql;
					echo $msg;
					//Write out to a script log file - if we have permissions to.
					file_put_contents("change-path.log", $msg,  FILE_APPEND | LOCK_EX);
				}
				
				
			}
		}
	} catch (Exception $e) {
		echo "\nSorry, we could not connect to the EHR system database. Please contact us at info AT medimage.co.nz, and mention the full error message: " . db_err_msg("exception",$e->getMessage());
	}
} else {
	echo "\nSorry, your installation is not working, and cannot connect to your EHR system. Are you sure your EHR Connector system is fully installed on this machine? If so, this is an issue which medimage.co.nz may need to know about. Please contact us at info AT medimage.co.nz, and mention that 'the " . (($json['useODBC']) ? 'ODBC' : 'SQL Server') .  " PHP functions' were not available.";	

}			

?>
