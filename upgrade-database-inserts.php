<?php
/* 

MedImage EHR Connector MedTech database inserts upgrader.

This script is for MedImage installer use after MedTech make any database schema changes to their ATTACHMENTSMGR table in MedTech Evolution.  This has only happened once since 2017, in later 2023, with the upgrade release 6.3 (version 17) of MedTech Evolution, however it could also happen in future.

You can run the script by running upgrade-database-inserts.bat in the Windows Filemanager on the Windows server that holds MedImage.

The script works by reading the last entered manual photo attachment, mimicing the data added, and then swapping over any fields with core MedImage-custom information e.g. the filename of the photo, it's name, it's size, and the author being "MedImage". The output SQL Insert statement is then copied over the configuration in config/ehrconnect.json, and future photos will be added with this template.

The script could still fail in future if 
a) MedTech completely change their database table
b) There are new data ID join fields which don't have any consistency between entries
	
But it should work in most cases where e.g. a new misc data field has been added.	
	
The second half of the functionality will ask the user whether they want to change historic MedImage photo entries, as well.
	
The output to this script is an interactive command-line message for the user.

*/

$ehrconnect_path = "C:\\MedImage\\addons\\ehrconnect\\";

include_once($ehrconnect_path . "db-wrapper.php");


//Read the config file to get parameters
$json_str = file_get_contents(realpath ($ehrconnect_path . 'config\\ehrconnect.json'));
$json = json_decode($json_str, true); // decode the JSON into an associative array
$json = backwards_compatible($json);	//ensure any old style entries are converted into the new style


echo "\n\nMedImage EHR Query Refresher for MedTech Evolution\n==================================================\n\nThis script will update the MedImage EHR Connector to match your MedTech Evolution installation, using data gathered by the last manual photo attachment added to MedTech's Attachments Manager interface. Note: This script should be run as an admin user.\n\nWe recommend that you upload a photo now, manually, inside the Medtech Attachments Manager.  This can be under any patient record.\n\nDo you want to continue? (y/N)";

if (!in_array(trim(fgets(STDIN)), array('y', 'Y'))) {
    echo 'Stopped' . "\n";
    exit(0);
}


if(db_code_checks($json)) {

	try {
		$dbh = db_connect($json);
		if(!$dbh) {
			$errmsg = db_err_msg($json);
			
			echo "\nSorry, we could not connect to the EHR system database. Please check your username, password, and other parameters.";
		} else {
		
			
			
			//Get the last manually entered photo on this server
			$sql = $json['testConnectionQuery'];	//Should be "SELECT TOP(1) * FROM ATTACHMENTSMGR ORDER BY ID DESC;";
			
			$success = true;
			
			//Run through each query
			echo "\nRunning query to find the latest Attachments Manager entry:  " . $sql;
			$db_result = db_query($json, $dbh, $sql) or die("Sorry, the MedImage Add-on could not query the EHR system. Please check your user has 'read' permissions. Trying to run the query: " . $sql);	
			
			//Collect the data into an associative PHP array.  WARNING: we are using the ODBC connection, rather than our db-wrapper. Using the SQL server connection may break this.
			$result = array();
			while($row = odbc_fetch_row($db_result)) {
				for ($j = 1; $j <= odbc_num_fields($db_result); $j++) {
				   $field_name = odbc_field_name($db_result, $j);
				   $value = odbc_result($db_result, $field_name);
				   $result[0][$field_name] = $value;
				}
			}
			
			//Useful test data below. Comment this line out.
			/*
			$result[0] = array("Id" => "39465", 
								"EntityId" => 17072,
								"WhenAdded" => "2024-02-08 14:30:43.263",
								"Description" => "test new ",
								"PathToFile" => "\\\\Gxhrs01\\phxge$\\Abc1234\\19-Jan-2024-08-44-35-small.jpg",
								"Note" => null,
								"IsActive" => 1,
								"Type" => ".jpg",
								"StorageMode" => 2,
								"AppModeId" => 2500332);		//Test data. And more fields. Not sure if NULL is value NULL or string "NULL", yet.
			*/
			
			if (!$result){
				echo "\nSorry, there don't appear to be any entries in the Attachments Manager.  Please add a photo attachment manually through the MedTech Attachments Window, under any patient record.";
				exit(0);
			} else {
				//Successful query, continue on...			
			}
			
			//Result now in this format: $result[0] array("fieldname" => "value", "fieldname" => "value)
			//and accessible from e.g. $result[0]['WhenAdded']
			

			$insert_field_sql = "(";	
			$insert_value_sql = "(";
			//Becomes e.g. INSERT INTO ATTACHMENTSMGR (EntityId, WhenAdded, Description, PathToFile, Note, FileSize,RowInactive, RowInsertComp, RowInsertUserId, RowInsertWhen, RowUpdateUserId, RowInsertLocationId,RowUpdateLocationId, RowUpdateWhen, RowUpdateComp, CreatedComputerId, CreatedBy, CreatedAt, CreatedLocationId,ModifiedComputerId, ModifiedBy, ModifiedAt,ModifiedLocationId,IsActive, Type,Author,StorageMode,AppModeId) VALUES('[patientID]','[currentDateTime]','MedImage Photo', '[fullphotopath]','[photoname]',[filelength],0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,1,'[currentDateTime]',1,1,1,'[currentDateTime]',1,1,'.jpg','[staffname]',2,2500332);
			
			$update_historic_sql = "UPDATE ATTACHMENTSMGR SET ";
			//Becomes e.g. UPDATE ATTACHMENTSMGR SET AppModeId = 2500332 WHERE Author = 'MedImage';
			
			$field_num = 0;
			$historic_query = false;		//by default nothing to change
			$historic_field_num = 0;
			
			echo "\nThe resulting fields and values are:\n";
				
			foreach($result[0] as $fieldname => $value) {
				echo "\nFieldname = " .  $fieldname . "  Value = " . $value;
				//Ignore the "Id" field
				if($fieldname != "Id") {
					if($field_num != 0) $insert_field_sql .= ", ";
					$insert_field_sql .= $fieldname;
					
					//Check if this is one of our known fieldnames that needs value replacement
					switch($fieldname) {
						case "EntityId":
							if($field_num != 0) $insert_value_sql .= ", ";
							$insert_value_sql .= "'[patientID]'";
						break;
						
						case "WhenAdded":
						case "RowInsertWhen":
						case "RowUpdateWhen":
						case "CreatedAt":
						case "ModifiedAt":
							if($field_num != 0) $insert_value_sql .= ", ";
							$insert_value_sql .= "'[currentDateTime]'";
						break;
						
						case "Description":
							if($field_num != 0) $insert_value_sql .= ", ";
							$insert_value_sql .= "'MedImage Photo'";
						break;
						
						case "PathToFile":
							if($field_num != 0) $insert_value_sql .= ", ";
							$insert_value_sql .= "'[fullphotopath]'";
						break;
						
						case "Note":
							if($field_num != 0) $insert_value_sql .= ", ";
							$insert_value_sql .= "'[photoname]'";
						break;
						
						case "FileSize":
							if($field_num != 0) $insert_value_sql .= ", ";
							$insert_value_sql .= "[filelength]";
						break;
						
						case "Type":
							//Handle different filetypes, but for the EHR Connector < 1.0.3
							if($field_num != 0) $insert_value_sql .= ", ";
							if(file_exists(realpath ($ehrconnect_path . 'config\\general-file-support.json')) ) {
								//We can use the file extension replacer
								$insert_value_sql .= "'[ext]'";
							} else {
								$insert_value_sql .= "'.jpg'";
							}
							
						break;
						
						case "Author":
							if($field_num != 0) $insert_value_sql .= ", ";
							$insert_value_sql .= "'MedImage'";
						break;
						
						case "IsActive":
							if($field_num != 0) $insert_value_sql .= ", ";
							$insert_value_sql .= "1";			//A 'true' value of 1
						break;
						
						//Note: set to 1, rather than 2 fixes a subtle non file-opening interface issue.  After some further feedback, we
						//may let this be read off the database, rather than being forced, but at least we know this will work for now. 
						case "StorageMode":
							if($field_num != 0) $insert_value_sql .= ", ";
							$insert_value_sql .= "1";			//A 'true' value of 1
						break;
						
					
						default:
							//No known fieldname: fill with existing data					
							if(is_null($value)) {
								if($field_num != 0) $insert_value_sql .= ", ";
								if($historic_field_num != 0) $update_historic_sql .= ", ";
								
								$insert_value_sql .= "NULL";	//A null without quotes
								$update_historic_sql .= $fieldname . " = NULL";
								$historic_field_num ++;
								$historic_query = true;
							} else {
							
								if(is_numeric($value)) {			//See https://www.php.net/manual/en/function.is-numeric.php
									if($field_num != 0) $insert_value_sql .= ", ";
									if($historic_field_num != 0) $update_historic_sql .= ", ";
									
									$insert_value_sql .= $value;	//A number without quotes
									$update_historic_sql .= $fieldname . " = " . $value;
									$historic_field_num ++;
									$historic_query = true;
								} else {
									if($field_num != 0) $insert_value_sql .= ", ";
									if($historic_field_num != 0) $update_historic_sql .= ", ";
									
									$insert_value_sql .= "'" . $value . "'";	//A string with quotes
									$update_historic_sql .= $fieldname . " = '" . $value . "'";
									$historic_field_num ++;
									$historic_query = true;
								}
							}
						break;
					}
					
					$field_num ++;
				}
				
				
			}
			
			$insert_field_sql .= ")";
			$insert_value_sql .= ")";
			$update_historic_sql .= " WHERE Author = 'MedImage';";
			
			$insert_sql = "INSERT INTO ATTACHMENTSMGR " . $insert_field_sql . " VALUES" . $insert_value_sql . ";";
						
			
			echo "\n\nWe can update the Insert Query stored by the EHR Connector to:\n\n" . $insert_sql . "\n\nBy comparison, the old Insert Query of the EHR Connector was:\n\n" . urldecode($json['insertAttachmentQuery']) . "\n\nDo you want to update the Insert Query? (y/N)";

			if (!in_array(trim(fgets(STDIN)), array('y', 'Y'))) {
				echo "\nSkipped\n";
			} else {
				$json['insertAttachmentQuery'] = $insert_sql;
				$str = json_encode($json, JSON_PRETTY_PRINT);
				if(file_put_contents(realpath($ehrconnect_path . 'config\\ehrconnect.json'), $str)) {
					echo "\nWritten the new EHR Connector configuration, successfully.  If you refresh your 'MedImage Server > Settings > EHR Connector' page, it should now be visible under 'Advanced > Insert attachment query' field.  Photos taken from now onwards will use this configuration.\n";
				} else {
					echo "\nSorry, there was a problem writing the new EHR Connector configuration. Please try running this script as an administrator user, e.g. 'right' click on this batch file in Windows File Manager, and select 'Run as administrator' ";
				}
			}
			
			
			//Set the config file with the standard new username
			
			
						
			if($historic_query == true) {
				//Now run a second query to change any historic entries		
				echo "\n\nThere may be a change needed to bring historic MedImage photos inline with the new configuration. To complete this we can run the SQL query:\n\n" . $update_historic_sql . "\n\nWe recommend that you back-up your full MedTech SQL-Server database prior to running this query.\n\nDo you want to update all historic MedImage photos with these new field values? (y/N)";

				if (!in_array(trim(fgets(STDIN)), array('y', 'Y'))) {
					echo 'Stopped' . "\n";
					exit(0);
				}
				
				echo "\nQuery:  " . $update_historic_sql;
				$result = db_query($json, $dbh, $update_historic_sql) or die("Sorry, the MedImage Add-on could not query the EHR system. Please check your user has 'read' permissions. Trying to run the query: " . $update_historic_sql);	
			
			}
			

						
			//Success!
			echo "\nSuccess";
			
				
		}
	} catch (Exception $e) {
		echo "\nSorry, we could not connect to the EHR system database. Please contact us at info AT medimage.co.nz, and mention the full error message: " . db_err_msg("exception",$e->getMessage());
	}
} else {
	echo "\nSorry, your installation is not working, and cannot connect to your EHR system. Are you sure your EHR Connector system is fully installed on this machine? If so, this is an issue which medimage.co.nz may need to know about. Please contact us at info AT medimage.co.nz, and mention that 'the " . (($json['useODBC']) ? 'ODBC' : 'SQL Server') .  " PHP functions' were not available.";	

}
	

?>
