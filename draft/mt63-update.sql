-- 1. The following query should be copied and pasted over the old query in:
-- MedImage Server Settings > EHR Connector tab > Advanced > Insert Attachment Query.  Then click Save

-- START OF QUERY TO COPY >
INSERT INTO ATTACHMENTSMGR (EntityId, WhenAdded, Description, PathToFile, Note, FileSize,RowInactive, RowInsertComp, RowInsertUserId, RowInsertWhen, RowUpdateUserId, RowInsertLocationId,RowUpdateLocationId, RowUpdateWhen, RowUpdateComp, CreatedComputerId, CreatedBy, CreatedAt, CreatedLocationId,ModifiedComputerId, ModifiedBy, ModifiedAt,ModifiedLocationId,IsActive, Type,Author,StorageMode,AppModeId) VALUES('[patientID]','[currentDateTime]','MedImage Photo', '[fullphotopath]','[photoname]',[filelength],0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,1,'[currentDateTime]',1,1,1,'[currentDateTime]',1,1,'.jpg','[staffname]',2,2500332);
-- < END OF QUERY TO COPY 

-- 2. To retrospectively update historic records to show in the Attachments Manager after the update, open your SQL Server and run and commit the following query:
--    Note: you may wish to confirm this dataset is correct, first, with: SELECT * FROM ATTACHMENTSMGR WHERE Author = 'MedImage';
--    We would also recommend creating a database backup prior to running any UPDATE commands.

UPDATE ATTACHMENTSMGR SET AppModeId = 2500332 WHERE Author = 'MedImage';

