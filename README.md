# medimage-addon-ehr-medtech-evolution
MedTech Evolution config for EHR Connector Add-on for MedImage



To create your own EHR connector, use this package as a template. The only files
you need to change:

1. ehrconnect.json    			- which should include queries specific to your database 
									and system tables. Make sure you have valid JSON
									(use an online json validator)
2. odbc\*             			- the ODBC driver .exe or .msi that your database requires
									 should be in here
3. medimage-installer.json    	- you should change the install commands to install your
									ODBC driver file silently



## Some useful system queries

Caution: Before running any of these queries, please back up your database.


**Importing from MT32 to MT Evo**

After importing an MT32 database into a fresh MT Evo system, the image attachments may not be set correctly. While running the upgrading script fully (by uploading a fresh photo through MedTech itself) will likely set these correctly, the following query may also be used to set some of the key constant values (which may be NULL):

```
UPDATE ATTACHMENTSMGR SET Type = '.jpg', Author = 'MedImage', StorageMode = 1 WHERE Note = 'Added by MedImage';
```

**To detect duplicate patient records**

Will give you a list of all of the duplicate patient records by NHID (these may need to be manually merged through the interface):
```
SELECT IsPatient, NHI, COUNT(*) FROM Patient GROUP BY IsPatient, NHI HAVING COUNT(*) > 1 AND IsPatient = 1;
```


**To allow for duplicates**

MedTech Evolution appears to only show individual NHIDs during the search, and what MedImage detects as duplicated NHID records may not be shown as two separate records.

One potential solution to this is to remove the barriers to the duplicate NHID errors within MedImage, but please use caution with this approach.  If there are large numbers of duplicates, which must be merged manually, this could simply push the problem further down the track.

Under MedImage Server Settings > EHR Connector > Advanced Options > Get identifier query:

Replace the default
```
SELECT ID AS PATIENTID FROM PATIENT WHERE PATIENT.ISPATIENT = 1 AND PATIENT.NHI = '[NHID]';
```

With
```
SELECT TOP (1) ID AS PATIENTID FROM PATIENT WHERE PATIENT.ISPATIENT = 1 AND PATIENT.NHI = '[NHID]' ORDER BY PATIENTID ASC;
```

or
```
SELECT TOP (1) ID AS PATIENTID FROM PATIENT WHERE PATIENT.ISPATIENT = 1 AND PATIENT.NHI = '[NHID]' ORDER BY PATIENTID DESC;
```

The 'DESC' option will use the latest entry in the table, by default, while the 'ASC' option will use the first entry in the table, by default.
